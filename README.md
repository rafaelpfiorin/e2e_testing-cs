# Front-end test automation - End to end

## How to use it

1. Install Visual Studio 2022 or later.
2. Clone this project with HTTPS option.
3. Browse 'Base2.sln' in Visual Studio to open the solution.
4. Add the 'Selenium.WebDriver.Geckodriver' Nuget package to IDE.
5. Go to some .cs file and click with right button to execute 'Run Tests'.

Enjoy!

﻿using NUnit.Framework;
using OpenQA.Selenium;

namespace AutomacaoTeste
{
    class MainPage : LogInOut
    {
        [Test]
        public void ComprimirSidebar()
        {
            Acesso();
            driver.FindElement(By.XPath("/html/body/div[2]/div[1]/div/i")).Click();

            Assert.IsFalse(driver.FindElement(By.ClassName("menu-text")).
                Text.Contains(" Minha Visão "));
        }
        [Test]
        public void AcessarOpcoesDaSidebar()
        {
            Acesso();

            driver.FindElement(By.XPath("/html/body/div[2]/div[1]/ul/li[1]/a/span")).Click();
            driver.FindElement(By.XPath("/html/body/div[2]/div[1]/ul/li[2]/a/span")).Click();
            driver.FindElement(By.XPath("/html/body/div[2]/div[1]/ul/li[3]/a/span")).Click();
            driver.FindElement(By.XPath("/html/body/div[2]/div[1]/ul/li[4]/a/span")).Click();
            driver.FindElement(By.XPath("/html/body/div[2]/div[1]/ul/li[5]/a/span")).Click();
        }
    }
}

﻿using NUnit.Framework;
using OpenQA.Selenium;

namespace AutomacaoTeste
{
    class LogInOut : Begin
    {
        public void Acesso()
        {
            driver.FindElement(By.Id("username")).SendKeys("Rafael.Fiori");
            driver.FindElement(By.XPath("//*[@id=\"login-form\"]/fieldset/input[2]")).Click();
            // entra com usuario e senha
            driver.FindElement(By.Id("password")).SendKeys("mbg");
            driver.FindElement(By.XPath("//*[@id=\"login-form\"]/fieldset/input[3]")).Click();
        }
        public void AcessoIncorreto()
        {
            driver.FindElement(By.Id("username")).SendKeys("Rafael.Fiori");
            driver.FindElement(By.XPath("//*[@id=\"login-form\"]/fieldset/input[2]")).Click();
            // entra com senha incorreta
            driver.FindElement(By.Id("password")).SendKeys("mbh");
            driver.FindElement(By.XPath("//*[@id=\"login-form\"]/fieldset/input[3]")).Click();


        }
        public void FimAcesso()
        {
            // realiza log out
            driver.FindElement(By.XPath("//*[@id=\"navbar-container\"]/div[2]/ul/li[3]/a/span")).Click();
            driver.FindElement(By.XPath("//*[@id=\"navbar-container\"]/div[2]/ul/li[3]/ul/li[4]/a")).Click();
        }

        [Test]
        public void LogarComSucesso()
        {
            Acesso();
        }
        [Test]
        public void LogarSemSucesso()
        {
            AcessoIncorreto();

            Assert.That(driver.FindElement(By.ClassName("alert-danger")).
                Text.Contains("usuário e a senha que você digitou não estão corretos"));
        }
        [Test]
        public void Deslogar()
        {
            Acesso();
            FimAcesso();
        }

    }
}

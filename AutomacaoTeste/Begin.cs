using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace AutomacaoTeste
{
    public class Begin
    {
        public IWebDriver driver;

        [SetUp]
        public void InicioTeste()
        {
            driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("https://mantis-prova.base2.com.br");

            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        [TearDown]
        public void FimDoTeste()
        {
            driver.Quit();
        }
    }
}